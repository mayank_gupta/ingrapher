class PathFinder:

	def __init__(self, maze):
		self.maze = maze
		self.min_x = 0
		self.max_x = len(maze[0])-1
		self.min_y = 0
		self.max_y = len(maze)-1
		self.solutions = []


	def neighbours(self, point):
		n = list()
		x = point[0]
		y = point[1]
		if self.maze[point[0]][point[1]] !=1: return n
		if x-1 >= self.min_x and self.maze[x-1][y] == 1:
			n.append((x-1,y)) #left
		if x+1 <= self.max_x and self.maze[x+1][y] == 1:
			n.append((x+1, y)) #right
		if y-1 >= self.min_y and self.maze[x][y-1] == 1:
			n.append((x, y-1)) #up
		if y+1 <= self.max_y and self.maze[x][y+1] == 1:
			n.append((x, y+1)) #down

		return n


	def dfs(self, start, goal, max_solutions=5):
		visited = set()
		stack = list()
		stack.append((start,[start]))
		solutions = 0

		while len(stack) != 0 and solutions <= max_solutions:
			point, path = stack.pop()

			if point==goal:
				self.solutions.append(path)
				solutions += 1
			
			else: 	
				visited.add(point)
				children = self.neighbours(point)
				for child in children:
					if child not in visited:
						current_path = path + [child]
						stack.append((child, current_path))


def find_path(maze, start, goal, max_solutions):
	p = PathFinder(maze)
	p.dfs(start, goal, max_solutions)
	print(list(sol for sol in p.solutions))


maze = [[1,1,1],[1,1,1],[1,0,1]]
find_path(maze, (1,0), (2,2), 5)
