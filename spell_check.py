import re, collections, os, sys
class SpellChecker:

	def words(self, text):
		return re.findall('[a-z]+', text.lower())

	def train(self,features):
		for f in features:
			self.model[f] += 1

	def __init__(self, filename):
		self.model = collections.defaultdict(lambda: 1)
		self.train(self.words(open(os.path.join(sys.path[0],filename)).read()))
		self.alphabet = 'abcdefghijklmnopqrstuvwxyz'

	def edits(self, word):
		splits = [(word[:i], word[i:]) for i in range(len(word) +1)]
		deletes_1 = [a + b[1:] for a,b in splits if b]
		transposes = [a + b[1] + b[0] + b[2:] for a,b in splits if len(b) >1]
		replaces = [a+c+b[1:] for a,b in splits for c in self.alphabet if b]
		inserts = [a+c+b for a,b in splits for c in self.alphabet]
		insert_split = set()
		for w in inserts:
			for i in range(len(w)+1):
				insert_split.add((w[:i],w[i:]))
		inserts2 = [a+c+b for a,b in insert_split for c in self.alphabet]
		return set(deletes_1 + transposes + replaces+inserts + inserts2)

	def correct(self, word):
		word_p = self.edits(word)
		return [possibility for possibility in word_p if possibility in self.model]

s = SpellChecker('dictionary.txt')
print(s.correct('marutix'))
print(s.correct('mauti'))
print(s.correct('maurti')) 
print(s.correct('maruty'))
print(s.correct('mrti'))