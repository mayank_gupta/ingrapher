class Rectangle:

	def __init__(self, x1, y1, x2, y2):
		self.points = list()
		self.points.append((x1,y1));
		self.points.append((x2, y1));
		self.points.append((x2, y2));
		self.points.append((x1, y2));
		self.x1 = x1
		self.x2 = x2
		self.y1 = y1
		self.y2 = y2

	def contains(self, x, y):
		if x >= min(self.x1, self.x2) and x<=max(self.x1,self.x2) and y >= min(self.y1, self.y2) and y<=max(self.y1, self.y2):
			return True
		else: 
			return False

def collide (ax1, ay1, ax2, ay2, bx1, by1, bx2, by2):
	rect_a = Rectangle(ax1, ay1, ax2, ay2)
	rect_b = Rectangle(bx1, by1, bx2, by2)
	print(rect_a.points)
	for point in rect_b.points:
		if rect_a.contains(point[0], point[1]):
			return True
	return False

print(collide(-2,2,-8,6,5,5,15,15))