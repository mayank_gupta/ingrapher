import collections

def longest (s):
    len_s = 0
    s_i = 0
    e_i = 0
    bitmap = collections.defaultdict(lambda: 0)

    for i in range(len(s)):

        if bitmap[s[i]] == 0:
            e_i += 1
            bitmap[s[i]] = 1

        elif bitmap[s[i]] == 1:
            len_s = max(e_i - s_i, len_s)
            while s[s_i] != s[i]:
                s_i += 1
                bitmap[s[i]] = 0;

            s_i += 1
            e_i += 1

    len_s = max(e_i-s_i, len_s)
    return len_s


tests = list()
tests.append('')
tests.append('A')
tests.append('AAABCDEE')
tests.append('AAAAAAABAAAAA')
tests.append('ABCDEFGHIJJ')
tests.append('AABCDEFG')
tests.append('AAABCDEEFGGH')

for test in tests:
    print(test, longest(test))
