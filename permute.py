class Ranker:

	def __init__(self, input):
		self.input = input
		self.input_length = len(input)
		self.used = [False for i in range(self.input_length)]
		self.permutations = list()
		self.generate_permutation()

	def permute (self, input, output, used, input_length, index):
		if input_length == index:
			self.permutations.append("".join(output))

		for i in range(input_length):
			if used[i] == True:
				continue
			output.append(input[i])
			used[i] = True
			self.permute(input, output, used, input_length, index+1)
			used[i] = False
			output.pop()

	def generate_permutation (self):
		self.permute(self.input, [], self.used, self.input_length,0)
		self.permutations.sort()

	def print_permutation (self):
		for string in self.permutations:
			print(string)

		print(len(self.permutations))

	def rank(self, word):
		if (len(word) != self.input_length): 
			return 0
		elif word in self.permutations:
			return self.permutations.index(word)+1
		else: return 0

def get_rank (l, word):
	x = Ranker(l)
	print(x.rank(word))


l = ['a','r','c']
get_rank(l,'rac')